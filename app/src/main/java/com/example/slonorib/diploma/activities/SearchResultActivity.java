package com.example.slonorib.diploma.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.slonorib.diploma.Models.Dish;
import com.example.slonorib.diploma.R;
import com.example.slonorib.diploma.adapters.DishAdapter;

import java.util.ArrayList;

public class SearchResultActivity extends ActionBarActivity {

    private DishAdapter dishAdapter;
    private ListView list;
    private ArrayList<Dish> dishes;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        initUI();
        setListeners();
        checkIntent();
        setupActionBar();
        fillList();
    }

    public void initUI(){
        list = (ListView) findViewById(R.id.listView);
        dishes = new ArrayList<>();
        dishAdapter = new DishAdapter(getApplicationContext(), dishes);
        list.setAdapter(dishAdapter);
    }

    public void setListeners(){
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), DetailRestActivity.class);
                startActivity(intent);
            }
        });
    }

    public void checkIntent(){
        Intent intent = getIntent();
        if(intent != null && intent.hasExtra("QUERY")){
            query = intent.getStringExtra("QUERY");
        }

    }

    public void setupActionBar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.item_header), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        if(query != null)
        getSupportActionBar().setTitle(query);
    }

    public void fillList(){
        Dish dish1 = new Dish();
        dish1.setName("Плескавица");
        dish1.setPrice(560);
        dish1.setDesc("Плескавица - блюдо в виде круглой плоской котлеты, популярное на Балканах");
        dish1.setPortion("600гр.");
        dish1.setRestaurant("Балкан Гриль");
        dish1.setType("Мясо");

        Dish dish2 = new Dish();
        dish2.setName("Шашлык из курицы");
        dish2.setPrice(320);
        dish2.setDesc("Прекрасный шашлык");
        dish2.setPortion("200гр.");
        dish2.setRestaurant("Берёзка");
        dish2.setType("Мясо");

        Dish dish3 = new Dish();
        dish3.setName("Стейк из говядины");
        dish3.setPrice(520);
        dish3.setDesc("Подаётся с жареной фасолью, картофельным пюре и кисло-сладким соусом");
        dish3.setPortion("400гр.");
        dish3.setRestaurant("Atmosphere");
        dish3.setType("Мясо");

        Dish dish4 = new Dish();
        dish4.setName("Котлета Егорище");
        dish4.setPrice(180);
        dish4.setDesc("Мясо, много мяса");
        dish4.setPortion("500гр.");
        dish4.setRestaurant("Балкан Гриль");
        dish4.setType("Мясо");

        Dish dish5 = new Dish();
        dish5.setName("Котлета Бездипломная");
        dish5.setPrice(950);
        dish5.setDesc("Такая же невероятная, как выданный вовремя диплом");
        dish5.setPortion("500гр.");
        dish5.setRestaurant("Балкан Гриль");
        dish5.setType("Мясо");

        dishes.add(dish1);
        dishes.add(dish5);
        dishes.add(dish2);
        dishes.add(dish3);
        dishes.add(dish4);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
