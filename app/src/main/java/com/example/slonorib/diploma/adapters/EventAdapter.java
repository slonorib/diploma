package com.example.slonorib.diploma.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.slonorib.diploma.Models.Event;
import com.example.slonorib.diploma.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by slonorib on 10.04.2015.
 */
public class EventAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Event> items;
    private TextView name;
    private TextView rest;
    private TextView restType;
    private TextView dates;
    private LinearLayout valuesLayout;
    private LinearLayout headerLayout;

    public EventAdapter(Context context, ArrayList<Event> items){
        this.context = context;
        this.items = items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;

        if(convertView != null) {
            view = convertView;
        }
        else{
            view = layoutInflater.inflate(R.layout.item_event, null);
        }

        name = (TextView) view.findViewById(R.id.name);
        rest = (TextView) view.findViewById(R.id.rest);
        restType = (TextView) view.findViewById(R.id.type);
        valuesLayout = (LinearLayout) view.findViewById(R.id.descLayout);
        headerLayout = (LinearLayout) view.findViewById(R.id.headerLayout);
        dates = (TextView) view.findViewById(R.id.dates);

        valuesLayout.setTag("h"+position);
        headerLayout.setTag("v"+position);

        name.setText(items.get(position).getName());
        rest.setText(items.get(position).getRestaurant());
        restType.setText(items.get(position).getRestType());
        dates.setText(items.get(position).getStartDate());
        setListeners(view, position);

        return view;
    }

    public void setListeners(final View v, final Integer position){

        final LinearLayout valuesLayout = (LinearLayout) v.findViewWithTag("v"+position);
        final LinearLayout headerLayout = (LinearLayout) v.findViewWithTag("h"+position);

        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valuesLayout.getVisibility() == View.GONE && headerLayout.getVisibility() == View.VISIBLE){
                    valuesLayout.setVisibility(View.VISIBLE);
                    headerLayout.setVisibility(View.GONE);
                }
                else{
                    valuesLayout.setVisibility(View.GONE);
                    headerLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        valuesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valuesLayout.getVisibility() == View.VISIBLE)
                    valuesLayout.setVisibility(View.GONE);
                headerLayout.setVisibility(View.VISIBLE);
            }
        });
    }
}
