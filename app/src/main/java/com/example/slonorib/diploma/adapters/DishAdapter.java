package com.example.slonorib.diploma.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.slonorib.diploma.Models.Dish;
import com.example.slonorib.diploma.R;

import java.util.ArrayList;

/**
 * Created by slonorib on 14.04.2015.
 */
public class DishAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Dish> items;
    private TextView name, price, rest, desc, portion, type;

    public DishAdapter(Context context, ArrayList<Dish> items){
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        v = LayoutInflater.from(context).inflate(R.layout.item_dish, null);

        name = (TextView) v.findViewById(R.id.name);
        price = (TextView) v.findViewById(R.id.price);
        rest = (TextView) v.findViewById(R.id.rest);
        desc = (TextView) v.findViewById(R.id.desc);
        portion = (TextView) v.findViewById(R.id.portion);
        type = (TextView) v.findViewById(R.id.type);

        name.setText(items.get(position).getName());
        price.setText(items.get(position).getPrice()+"р");
        rest.setText(items.get(position).getRestaurant());
        desc.setText(items.get(position).getDesc());
        portion.setText(items.get(position).getPortion());
        type.setText(items.get(position).getType());

        return v;
    }
}
