package com.example.slonorib.diploma.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.slonorib.diploma.Models.Rest;
import com.example.slonorib.diploma.R;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by slonorib on 09.04.2015.
 */
public class RestAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Rest> rests;
    TextView tvName;
    TextView tvTypes;
    TextView tvAddress;
    TextView tvDistance;

    public RestAdapter(Context context, ArrayList<Rest> restaurants) {
        this.context = context;
        this.rests = restaurants;
    }

    @Override
    public int getCount() {
        return rests.size();
    }

    @Override
    public Object getItem(int position) {
        return rests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        v = LayoutInflater.from(context).inflate(R.layout.item_rest, null);

        tvName = (TextView) v.findViewById(R.id.name);
        tvTypes = (TextView) v.findViewById(R.id.type);
        tvAddress = (TextView) v.findViewById(R.id.address);
        tvDistance = (TextView) v.findViewById(R.id.distance);

        tvName.setText(rests.get(position).getName());
        tvTypes.setText(rests.get(position).getType());
        tvAddress.setText(rests.get(position).getAddress());

        Random r = new Random();
        int rand = r.nextInt(800 - 50) + 50;

        tvDistance.setText(rand+"м");

        return v;
    }
}
