package com.example.slonorib.diploma.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.slonorib.diploma.Models.Event;
import com.example.slonorib.diploma.R;
import com.example.slonorib.diploma.activities.DetailRestActivity;
import com.example.slonorib.diploma.adapters.EventAdapter;

import java.util.ArrayList;

public class EventFragment extends Fragment {

    private ArrayList<Event> events;
    private EventAdapter adapter;
    private ListView listView;
    private LinearLayout headerLayout;
    private LinearLayout valuesLayout;

    public EventFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event, container, false);

        initUI(rootView);
        setListeners();

        return rootView;
    }

    public void initUI(View view){
        listView = (ListView) view.findViewById(R.id.listView);
        events = new ArrayList<>();
        addTestData();
        adapter = new EventAdapter(getActivity().getApplicationContext(), events);
        listView.setAdapter(adapter);
    }

    public void setListeners(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getApplicationContext(), DetailRestActivity.class);
                startActivity(intent);
            }
        });

    }

    public void addTestData(){
        Event event1 = new Event();
        event1.setName("Скидки на выпечку 20%");
        event1.setRestaurant("Банковский Клуб");
        event1.setRestType("Ресторан");
        event1.setStartDate("01.03.15 - 01.06.15");
        events.add(event1);

        Event event2 = new Event();
        event2.setName("Концерт группы Ленинград");
        event2.setRestaurant("Максимилианс");
        event2.setRestType("Ресторан");
        event2.setStartDate("12.06.15 в 22:00");
        events.add(event2);

        Event event3 = new Event();
        event3.setName("Сэндвич со скидкой 50%");
        event3.setRestaurant("Subway");
        event3.setRestType("Фастфуд");
        event3.setStartDate("Каждый четверг");
        events.add(event3);

        Event event4 = new Event();
        event4.setName("Шведский стол");
        event4.setRestaurant("Егорка");
        event4.setRestType("Кафе");
        event4.setStartDate("10.04.15 - 30.04.15");
        events.add(event4);

        Event event5 = new Event();
        event5.setName("2-ой кофе бесплатно");
        event5.setRestaurant("Максим");
        event5.setRestType("Кофейня");
        event5.setStartDate("10.03.15 - 35.04.15");
        events.add(event5);

        Event event6 = new Event();
        event6.setName("Бесплатные соусы к пицце");
        event6.setRestaurant("Артишок");
        event6.setRestType("Караоке-клуб");
        event6.setStartDate("Каждый день после 22:00");
        events.add(event6);
    }

}