package com.example.slonorib.diploma.activities;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import com.example.slonorib.diploma.Models.Dish;
import com.example.slonorib.diploma.R;
import com.example.slonorib.diploma.adapters.DishMenuAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DishMenuActivity extends ActionBarActivity {

    private ExpandableListView listView;
    private DishMenuAdapter dishMenuAdapter;
    private List<String> headers;
    private HashMap<String, List<Dish>> hashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_menu);

        initUI();
        fillValues();
        setupActionBar();
    }

    public void initUI(){
        listView = (ExpandableListView) findViewById(R.id.expandableListView);
        headers = new ArrayList<>();
        hashMap = new HashMap<>();
        dishMenuAdapter = new DishMenuAdapter(this,headers,hashMap);
        listView.setAdapter(dishMenuAdapter);
    }

    public void setupActionBar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.item_header), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle(R.string.title_activity_dish_menu);
    }

    public void fillValues(){
        headers.add("Салаты");
        headers.add("Закуски");
        headers.add("Супы");
        headers.add("Основные блюда");
        headers.add("Десерты");
        headers.add("Напитки");

        List<Dish> salads = new ArrayList<>();

        Dish salad1 = new Dish();
        salad1.setName("Салат Греческий");
        salad1.setPrice(250);
        salad1.setPortion("300гр");
        salad1.setDesc("Греческий салат с помидорками. Ом ном ном");
        salads.add(salad1);

        Dish salad2 = new Dish();
        salad2.setName("Салат Шопский");
        salad2.setPrice(200);
        salad2.setPortion("280гр");
        salad2.setDesc("Классический восточноевропейский салат");
        salads.add(salad2);

        Dish salad3 = new Dish();
        salad3.setName("Салат Цезарь");
        salad3.setPrice(450);
        salad3.setPortion("200гр");
        salad3.setDesc("Одно из самых известных блюд североамериканской кухни");
        salads.add(salad3);

        List<Dish> drinks = new ArrayList<>();
        Dish drink1 = new Dish();
        drink1.setName("Яблочный сок");
        drink1.setPrice(50);
        drink1.setPortion("500мл");
        drink1.setDesc("Сок из свежих яблок");
        drinks.add(drink1);

        Dish drink2 = new Dish();
        drink2.setName("Апельсиновый сок");
        drink2.setPrice(50);
        drink2.setPortion("500мл");
        drink2.setDesc("Сок из свежих апельсинок");
        drinks.add(drink2);

        hashMap.put(headers.get(0), salads);
        hashMap.put(headers.get(5), drinks);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
