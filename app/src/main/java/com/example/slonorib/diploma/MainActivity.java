package com.example.slonorib.diploma;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;

import com.example.slonorib.diploma.fragments.EventFragment;
import com.example.slonorib.diploma.fragments.RestFragment;
import com.example.slonorib.diploma.fragments.MapFragment;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private FragmentManager fragmentManager;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

    }

    private void initUI(){
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment = null;
        fragmentManager = getSupportFragmentManager();
        switch (position){
            case 0:
                fragment = new MapFragment();
                getSupportActionBar().setTitle(R.string.title_section1);
                break;
            case 1:
                fragment = new RestFragment();
                getSupportActionBar().setTitle(R.string.title_section2);
                break;
            case 2:
                fragment = new EventFragment();
                getSupportActionBar().setTitle(R.string.title_section3);
                break;
        }
        if(fragment != null)
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }
}
