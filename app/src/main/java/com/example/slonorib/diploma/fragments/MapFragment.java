package com.example.slonorib.diploma.fragments;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.slonorib.diploma.R;
import com.example.slonorib.diploma.activities.SearchActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private LinearLayout searchLayout;
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private LatLng myLocation;
    private double defaultLat = 57.151734;
    private double defLon = 65.539075;

    public MapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        searchLayout = (LinearLayout) rootView.findViewById(R.id.searchLayout);
        setListeners();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initMap();
        onMapReady(map);
    }

    public void initMap() {
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapView);
        map = mapFragment.getMap();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        setupMap();

        map.addMarker(new MarkerOptions()
                .position(new LatLng(57.152932, 65.569069))
                .title("Балкан Гриль"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(57.149357, 65.560595))
                .title("Тортшер"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(57.148199, 65.575368))
                .title("Пицца Мия"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(57.152977, 65.564446))
                .title("Максимиллианс"));
    }

    public void setupMap() {
        getMyLocation();
        if (myLocation != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(myLocation)
                    .zoom(15)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1, null);
        } else {
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(defaultLat, defLon))
                        .zoom(15)
                        .build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1, null);
              }
        map.getUiSettings().setMyLocationButtonEnabled(true);
    }

    public void getMyLocation() {
        map.setMyLocationEnabled(true);
        map.setBuildingsEnabled(true);
        map.setIndoorEnabled(true);

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            myLocation = new LatLng(latitude, longitude);
        }
    }

    public void setListeners(){
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), SearchActivity.class);
                startActivity(intent);
            }
        });
    }
}