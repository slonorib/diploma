package com.example.slonorib.diploma.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.slonorib.diploma.Models.Rest;
import com.example.slonorib.diploma.R;
import com.example.slonorib.diploma.activities.DetailRestActivity;
import com.example.slonorib.diploma.adapters.RestAdapter;

import java.util.ArrayList;

public class RestFragment extends Fragment {

    private ListView listView;
    private RestAdapter restAdapter;
    private ArrayList<Rest> rests;


    public RestFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rest, container, false);

        initUI(rootView);
        setListeners();

        return rootView;
    }

    private void initUI(View view) {
        rests = new ArrayList<>();
        addTestData();
        listView = (ListView) view.findViewById(R.id.listView);
        restAdapter = new RestAdapter(getActivity().getApplicationContext(), rests);
        listView.setAdapter(restAdapter);
    }

    public void setListeners(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getApplicationContext(), DetailRestActivity.class);
                Rest rest = (Rest) restAdapter.getItem(position);
                intent.putExtra("REST", rest);
                startActivity(intent);
            }
        });
    }

    private void addTestData(){
        Rest rest1 = new Rest();
        rest1.setName("Балкан Гриль");
        rest1.setAddress("Ул. Максима Горького, 54");
        rest1.setType("Ресторан");
        rests.add(rest1);

        Rest rest2 = new Rest();
        rest2.setName("Дон Хулио");
        rest2.setAddress("Ул. Первомайская, 11");
        rest2.setType("Ресторан");
        rests.add(rest2);

        Rest rest3 = new Rest();
        rest3.setName("Берёзка");
        rest3.setAddress("7 км Червишевского тракта, 8");
        rest3.setType("Кафе");
        rests.add(rest3);

        Rest rest4 = new Rest();
        rest4.setName("Тортшер");
        rest4.setAddress("Ул. Максима Горького, 14");
        rest4.setType("Кафе");
        rests.add(rest4);

        Rest rest5 = new Rest();
        rest5.setName("Егорка");
        rest5.setAddress("Ул. Тюменская, 5");
        rest5.setType("Кафе");
        rests.add(rest5);

        Rest rest6 = new Rest();
        rest6.setName("Георгий");
        rest6.setAddress("Ул. Весенняя, 34");
        rest6.setType("Ресторан");
        rests.add(rest6);
    }
}