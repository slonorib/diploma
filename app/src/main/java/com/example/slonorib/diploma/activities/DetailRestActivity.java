package com.example.slonorib.diploma.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.slonorib.diploma.MainActivity;
import com.example.slonorib.diploma.Models.Rest;
import com.example.slonorib.diploma.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class DetailRestActivity extends ActionBarActivity implements OnMapReadyCallback {

    private LinearLayout headerLayout;
    private LinearLayout valuesLayout;
    private MapFragment mapFragment;
    private GoogleMap map;
    private Rest rest;
    private Button button;

    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rest);

        initUI();
        setListeners();
        checkIntent();
        onMapReady(map);
        setupMap();
        setupActionBar();
        loadPhoto();
    }

    public void initUI(){
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
        .build();
        ImageLoader.getInstance().init(config);
        img = (ImageView) findViewById(R.id.imageView16);

        headerLayout = (LinearLayout) findViewById(R.id.layoutTimeHeader);
        valuesLayout = (LinearLayout) findViewById(R.id.layoutTimeValues);
        button = (Button) findViewById(R.id.button);
        mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
        map = mapFragment.getMap();
    }

    public void loadPhoto(){
        String url = "http://photowithlove.com/%D1%81%D0%B2%D0%B0%D0%B4%D0%B5%D0%B1%D0%BD%D0%BE%D0%B5-%D1%84%D0%BE%D1%82%D0%BE/2013/06/%D0%A0%D0%B5%D1%81%D1%82%D0%BE%D1%80%D0%B0%D0%BD-%D0%93%D1%83%D1%81%D1%8F%D1%82%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2.jpg";
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage(url, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                img.setImageBitmap(loadedImage);
            }
        });
    }

    public void setListeners(){
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valuesLayout.getVisibility() == View.GONE && headerLayout.getVisibility() == View.VISIBLE) {
                    valuesLayout.setVisibility(View.VISIBLE);
                    headerLayout.setVisibility(View.GONE);
                } else {
                    valuesLayout.setVisibility(View.GONE);
                    headerLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        valuesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valuesLayout.getVisibility() == View.VISIBLE)
                    valuesLayout.setVisibility(View.GONE);
                headerLayout.setVisibility(View.VISIBLE);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DishMenuActivity.class);
                startActivity(intent);
            }
        });
    }

    public void checkIntent() {
        Intent intent = getIntent();
        if(intent != null && intent.hasExtra("REST")) {
            rest = (Rest) intent.getExtras().get("REST");
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setScrollGesturesEnabled(false);
        map.getUiSettings().setZoomGesturesEnabled(false);
        //map.getUiSettings().setZoomControlsEnabled(true);

        map.addMarker(new MarkerOptions()
                .position(new LatLng(57.152932, 65.569069))
                .title("Балкан Гриль"));
    }

    public void setupMap(){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(57.152932, 65.569069))
                .zoom(15)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1, null);
    }

    public void setupActionBar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.item_header), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        if(rest != null)
            getSupportActionBar().setTitle(rest.getName());
        else
            getSupportActionBar().setTitle("Ресторан");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
